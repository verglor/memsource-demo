package memsource.demo

class SetupController {

    ConfigService configService
    MemsourceService memsourceService

    def index() {
        render (view: 'index', model: [username: configService.username])
    }

    def save(String username, String password) {

        try {
            configService.setUsernameAndPassword(username, password)
            // If new credentials were stored then evict cached token
            memsourceService.evictToken()
            flash.message = message(code: 'controller.setup.save.success')
        } catch (e) {
            log.error e.message, e
            flash.errors = [message(code: 'controller.setup.save.error')]
        }
        redirect action: 'index'

    }

}
