package memsource.demo.api

import grails.converters.JSON
import memsource.demo.*

class ApiProjectController {

    static namespace = 'api'

    MemsourceService memsourceService

    def list() {

        response.contentType = 'application/json'

        try {
            List<Project> projects = memsourceService.projects
            render (
                    text: [
                        size: projects.size(),
                        items: projects
                    ] as JSON
            )
        } catch (e) {
            log.error e.message, e
            render (
                    text: [message: message(code: 'controller.api.project.list.error')] as JSON,
                    status: 500,
            )
        }

    }
}
