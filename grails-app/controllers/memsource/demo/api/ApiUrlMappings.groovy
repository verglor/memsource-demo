package memsource.demo.api

class ApiUrlMappings {

    static mappings = {

        group "/api", {
            "/projects" (namespace: 'api', controller: 'apiProject', action: 'list', method: 'GET')
        }

    }

}


