package memsource.demo

class UrlMappings {

    static mappings = {

        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'setup')
        "/projects"(controller: 'project')
        "500"(view: '/error')
        "404"(view: '/notFound')

    }

}
