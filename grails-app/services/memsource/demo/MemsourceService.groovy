package memsource.demo

import grails.plugin.cache.*

/**
 * Service providing basic access to Memsource API.
 */
// TODO: Handle errors with custom exceptions
class MemsourceService {

    MemsourceClient memsourceClient
    ConfigService configService

    /**
     * Get API token to access Memsource API according to provided <code>username</code> and <code>password</code>.
     * Token will be cached until {@link #evictToken()} is called.
     *
     * @return API token
     */
    @Cacheable(value = 'token', key = {'token'})
    String getToken() {

        memsourceClient.login(configService.username, configService.password).token

    }

    /**
     * Evict token form cache.
     */
    @CacheEvict(value = 'token', key = {'token'})
    void evictToken() {
    }

    /**
     * List of projects of user that provided the API token.
     *
     * @return List of {@link Project}s.
     */
    // TODO: Handle pagination
    List<Project> getProjects() {

        memsourceClient.listProjects(token).content.collect { Map data ->
            new Project(data.subMap(['name', 'status', 'sourceLang', 'targetLangs']))
        }

    }

}
