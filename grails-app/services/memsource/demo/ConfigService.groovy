package memsource.demo

import grails.gorm.transactions.Transactional

/**
 * Service providing access to settings stored in DB.
 */
@Transactional
class ConfigService {

    /**
     * Enumeration of supported setting keys
     */
    static enum Key { USERNAME, PASSWORD }

    /**
     * Retrieve setting from DB.
     *
     * @param key Key of setting to be retrived
     * @return Value of setting for provided key or null if setting with that key does not exist.
     */
    String getValue(key) {
        Config.findByConfigKey(key as String)?.configValue
    }

    /**
     * Store setting to DB.
     * @param key Key of setting to be stored
     * @param value Value of setting to be stored
     */
    void setValue(key, value) {
        def config = Config.findOrCreateByConfigKey(key as String)
        config.configValue = value as String
        config.save(failOnError: true)
    }

    /**
     * Retrieve username setting from DB.
     *
     * @return Username setting
     */
    String getUsername() {
        getValue(Key.USERNAME)
    }

    /**
     * Store username to DB.
     *
     * @param username Username setting to store
     */
    void setUsername(String username) {
        setValue(Key.USERNAME, username)
    }

    /**
     * Retrieve password setting from DB.
     *
     * @return Password setting
     */
    String getPassword() {
        getValue(Key.PASSWORD)
    }

    /**
     * Store password to DB.
     *
     * @param password Password setting to store
     */
    void setPassword(String password) {
        setValue(Key.PASSWORD, password)
    }

    /**
     * Store both username and password to DB in one transaction.
     *
     * @param username Username setting to store
     * @param password Password setting to store
     */
    void setUsernameAndPassword(String username, String password) {
        setUsername(username)
        setPassword(password)
    }

}
