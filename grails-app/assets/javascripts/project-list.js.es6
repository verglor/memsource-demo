$(() => {

    const tableBodyTemplate = Handlebars.compile(`
        {{#each items}}
        <tr>
            <td><em>{{name}}</em></td>
            <td><span class="badge badge-status badge-status-{{status}}">{{status}}</span></td>
            <td><span class="badge badge-lang bg-primary">{{sourceLang}}</span> →
            {{#each targetLangs}}
                <span class="badge badge-lang bg-secondary">{{this}}</span>
            {{/each}}
            </td>
        </tr>
        {{/each}}
    `);

    const errorTemplate = Handlebars.compile(`
        <div class="alert alert-danger m-2" role="alert">
            {{message}}
        </div>
    `);

    $.getJSON('/api/projects')
      .done(data => {
          console.info("Successfully retrieved projects from backend:", data.size);
          $('#project-table-body').html(tableBodyTemplate(data));
      })
      .fail((jqXHR, textStatus) => {
          console.error('Failed to retrieve projects from backend:', jqXHR.status, textStatus);
          $('#container-alerts').html(errorTemplate({
              message: (jqXHR.responseJSON && jqXHR.responseJSON.message) || 'Failed to communicate with server'
          }));
          $('#project-table-body').html('');
      });

});
