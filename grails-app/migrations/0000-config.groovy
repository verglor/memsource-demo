databaseChangeLog = {

    changeSet(author: "jaroslav.kostal", id: "1") {
        createTable(tableName: "CONFIG") {
            column(name: "CONFIG_KEY", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "CONFIG_VALUE", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
        addPrimaryKey(columnNames: "CONFIG_KEY", constraintName: "CONSTRAINT_7", tableName: "CONFIG")
    }

}
