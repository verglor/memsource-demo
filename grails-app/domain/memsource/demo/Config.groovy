package memsource.demo

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class representing configuration setting.
 *
 * Each record represents setting with unique string key and arbitrary string value.
 */
@EqualsAndHashCode
@ToString
class Config {

    /**
     * Key of the configuration setting.
     * It's unique primary key.
     * Field name is prefixed by <code>config</code> to prevent conflicts with SQL reserved word <code>key</code>.
     */
    String configKey

    /**
     * Value of the configuration setting.
     * Field name is prefixed by <code>config</code> to prevent conflicts with SQL reserved word <code>value</code>.
     */
    String configValue

    static mapping = {
        version false
        // Use setting's key as natural primary key instead of surrogate id
        id generator: 'assigned', name: 'configKey'
    }

    static constraints = {
        // Setting's key cannot be blank and by default is not nullable and unique
        configKey blank: false
    }

}
