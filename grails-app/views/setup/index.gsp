<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="view.setup.index.title"/></title>
</head>
<body>

<h1 class="text-center"><g:message code="view.setup.index.title"/></h1>

    <section class="row">


        <g:form action="save" method="post" class="mx-auto">
            <div class="form-group">
                <label for="input-username"><g:message code="_.username.label"/></label>
                <input type="text" class="form-control" name="username" value="${username}" id="input-username" required>
            </div>
            <div class="form-group">
                <label for="input-password"><g:message code="_.password.label"/></label>
                <input type="password" class="form-control" name="password" id="input-password" required>
            </div>
            <button type="submit" class="btn btn-primary"><g:message code="_.submit.label"/></button>
        </g:form>

    </section>

</body>
</html>
