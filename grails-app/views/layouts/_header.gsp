<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/#"><asset:image src="logo-dark.png" alt="Memsource Logo" width="250"/></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul id="nav-items" class="navbar-nav mr-auto">
            <tmpl:/layouts/nav-item controller="setup"/>
            <tmpl:/layouts/nav-item controller="project"/>
        </ul>
    </div>
</nav>
