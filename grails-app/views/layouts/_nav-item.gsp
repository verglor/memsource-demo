<li class="nav-item mx-2 ${controllerName == controller ? 'active' : ''}">
    <g:link controller="${controller}" action="" class="nav-link"><g:message code="nav.${controller}.label"/></g:link>
</li>
