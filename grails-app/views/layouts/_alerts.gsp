<div id="container-alerts" class="container">

    <g:if test="${flash.message}">
        <div class="alert alert-primary m-2" role="alert">
            ${flash.message}
        </div>
    </g:if>

    <g:each var="error" in="${flash.errors}">
        <div class="alert alert-danger m-2" role="alert">
            ${error}
        </div>
    </g:each>

</div>
