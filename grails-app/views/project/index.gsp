<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="view.project.index.title"/></title>
</head>
<body>

<h1 class="text-center"><g:message code="view.project.index.title"/></h1>

<section class="row colset-2-its">

    <div class="container mt-4">

        <div class="table-reponsive">
            <table class="table table-striped" id="project-table">
                <thead class="table-primary">
                <tr>
                    <th scope="col"><g:message code="view.project.index.table.header.name.label"/></th>
                    <th><g:message code="view.project.index.table.header.status.label"/></th>
                    <th scope="col"><g:message code="view.project.index.table.header.langs.label"/></th>
                </tr>
                </thead>
                <tbody id="project-table-body">
                    <tr>
                        <th colspan="3" class="text-center"><tmpl:/layouts/spinner/></th>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.6/handlebars.min.js" integrity="sha512-zT3zHcFYbQwjHdKjCu6OMmETx8fJA9S7E6W7kBeFxultf75OPTYUJigEKX58qgyQMi1m1EgenfjMXlRZG8BXaw==" crossorigin="anonymous"></script>
<asset:javascript src="project-list.js.es6" asset-defer="true" />


</body>
</html>
