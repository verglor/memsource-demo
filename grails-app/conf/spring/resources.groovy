import memsource.demo.MemsourceClient

beans = {
    memsourceClient(MemsourceClient) {
        baseUrl = grailsApplication.config.app.memsource.api.url ?: MemsourceClient.DEFAULT_BASE_URL
    }
}
