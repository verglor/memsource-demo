ruleset {

    ruleset('rulesets/basic.xml') {
        exclude 'EmptyMethod'
    }
    ruleset('rulesets/braces.xml') {
        exclude 'IfStatementBraces'
    }
    ruleset('rulesets/concurrency.xml')
    ruleset('rulesets/convention.xml') {
        exclude 'NoDef'
    }
    ruleset('rulesets/design.xml') {
        exclude 'Instanceof'
    }
//    ruleset('rulesets/dry.xml')
//    ruleset('rulesets/enhanced.xml')
    ruleset('rulesets/exceptions.xml')
    ruleset('rulesets/formatting.xml') {
        exclude 'ClassJavadoc'
        exclude 'ConsecutiveBlankLines'
        SpaceAroundMapEntryColon {
            characterAfterColonRegex = /\s/
        }
        LineLength {
            length = 150
        }
    }
    ruleset('rulesets/generic.xml')
    ruleset('rulesets/grails.xml')
    ruleset('rulesets/groovyism.xml')
    ruleset('rulesets/imports.xml') {
        exclude 'NoWildcardImports'
    }
    ruleset('rulesets/jdbc.xml')
    ruleset('rulesets/junit.xml')
    ruleset('rulesets/logging.xml')
    ruleset('rulesets/naming.xml') {
        exclude 'FactoryMethodName'
    }
    ruleset('rulesets/security.xml')
    ruleset('rulesets/serialization.xml')
    ruleset('rulesets/size.xml')
    ruleset('rulesets/unnecessary.xml') {
        exclude 'UnnecessaryGString'
        exclude 'UnnecessaryReturnKeyword'
    }
    ruleset('rulesets/unused.xml')

}
