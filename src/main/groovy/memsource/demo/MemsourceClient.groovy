package memsource.demo

import groovyx.net.http.RESTClient

/**
 * Basic Memsource API client.
 *
 * Thin wrapper around Memsource REST API independent on this project.
 */
// TODO: Replace with generated client form OpenAPI specification.
class MemsourceClient {

    static final String DEFAULT_BASE_URL = 'https://cloud.memsource.com/web'

    String baseUrl = DEFAULT_BASE_URL

    /**
     *  Login to Memsource API with provided <code>username</code> and <code>password</code>.
     *
     * @param username Username of user permitted to access API
     * @param password Password of user permitted to access API
     * @return JSON response
     *
     * @see {<a href="https://cloud.memsource.com/web/docs/api#operation/login">Memsource API docs</a>}
     */
    Map login(String username, String password) {

        new RESTClient("${baseUrl}/api2/v1/auth/login")
                .post(
                        requestContentType: 'application/json',
                        body: [
                            userName: username,
                            password: password,
                        ]
                )
                .data

    }

    /**
     * List of projects of user that provided the API <code>token</code>.
     *
     * @param token API token
     * @return JSON response
     *
     * @see {<a href="https://cloud.memsource.com/web/docs/api#operation/listProjects">Memsource API docs</a>}
     */
    Map listProjects(String token) {

        new RESTClient("${baseUrl}/api2/v1/projects")
                .get(query: [token: token])
                .data

    }

}
