package memsource.demo

import groovy.transform.Canonical

/**
 * Data class representing Memsource project.
 */
@Canonical
class Project {

    /**
     * Name of the project.
     */
    String name

    /**
     * Status of the project.
     */
    String status

    /**
     * 2-letter shortcut of source language.
     */
    String sourceLang

    /**
     * List of 2-letter shortcuts of target languages.
     */
    List<String> targetLangs = []

}
