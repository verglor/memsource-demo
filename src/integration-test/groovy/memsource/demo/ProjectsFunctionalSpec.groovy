package memsource.demo

import grails.testing.mixin.integration.Integration
import grails.transaction.*

import geb.spock.*
import memsource.demo.mock.MemsourceMockServer
import memsource.demo.pages.ProjectsPage
import memsource.demo.pages.SetupPage
import spock.lang.Shared
import static memsource.demo.mock.MemsourceMockServer.*

@Integration
@Rollback
class ProjectsFunctionalSpec extends GebSpec {

    @Shared
    MemsourceMockServer memsourceMockServer = MemsourceMockServer.instance

    void "test project listing with correct credentials"() {

        setup:"Store correct credentials on setup page"
            to SetupPage
            storeConfig(USERNAME, PASSWORD)

        when:"Go to projects page and wait until ajax request for list of project finishes"
            to ProjectsPage
            waitForProjectsLoaded()

        then:"Check number of projects displayed in table"
            rows.size() == PROJECT_COUNT

        and:"Check content of the first project in table"
            rows[0].name.text() == FIRST_PROJECT.name
            rows[0].status.text() == FIRST_PROJECT.status
            rows[0].langs.text() ==
                "${FIRST_PROJECT.sourceLang} → ${FIRST_PROJECT.targetLangs.join(' ')}"

        and:"No message is displayed"
            alerts.all.empty

    }

    void "test project listing with incorrect credentials"() {

        setup:"Store incorrect credentials on setup page"
            to SetupPage
            storeConfig('incorrect', 'credentials')

        when:"Go to projects page and wait until ajax request for list of project finishes"
            to ProjectsPage
            waitForProjectsLoaded()

        then:"Table is empty since no projects were retrieved"
            rows.empty

        and:"Error message is displayed"
            alerts.errors.size() == 1
            alerts.errors.firstElement().text == 'Failed to retrieve projects. Please check that you have provided correct credentials on the setup page.'

    }

}
