package memsource.demo.pages

import geb.Page

class SetupPage extends Page {

    static url = '/'

    static at = { title == "Setup configuration" }

    static content = {
        navigation { module(NavigationModule) }
        alerts { module(AlertsModule) }
        usernameInput { $("input[name=username]") }
        passwordInput { $("input[name=password]") }
        submitButton { $("button", type: "submit") }
    }

    void storeConfig(String username, String password) {
        usernameInput.value(username)
        passwordInput.value(password)
        submitButton.click()
    }

}
