package memsource.demo.pages

import geb.Module

class AlertsModule extends Module {

    static content = {
        container { $('#container-alerts') }
        all(required: false) { container.find('div.alert') }
        errors(required: false) { container.find('div.alert-danger') }
        messages(required: false) { container.find('div.alert-primary') }
    }

}
