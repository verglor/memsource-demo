package memsource.demo.pages

import geb.Module

class ProjectRowModule extends Module {

    static content = {
        cell { $('td', it)}
        name { cell(0) }
        status { cell(1) }
        langs { cell(2) }
    }

}
