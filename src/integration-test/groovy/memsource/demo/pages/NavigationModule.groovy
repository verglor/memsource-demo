package memsource.demo.pages

import geb.Module

class NavigationModule extends Module {

    static content = {
        container { $('#nav-items') }
        setupLink { container.find('li.nav-item a', text: 'Setup') }
        projectsLink { container.find('li.nav-item a', text: 'Projects') }
    }

}
