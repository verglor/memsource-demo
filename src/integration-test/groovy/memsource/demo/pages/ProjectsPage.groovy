package memsource.demo.pages

import geb.Page

class ProjectsPage extends Page {

    static url = '/projects'

    static at = { title == "List of projects" }

    static content = {
        navigation { module(NavigationModule) }
        alerts { module(AlertsModule) }
        tableBody { $('#project-table-body') }
        rows { tableBody.children('tr').moduleList(ProjectRowModule) }
        spinner(required: false) { tableBody.find('div.lds-facebook') }
    }

    void waitForProjectsLoaded() {
        waitFor { spinner.empty }
    }

}
