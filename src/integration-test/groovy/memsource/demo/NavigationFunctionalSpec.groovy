package memsource.demo

import geb.spock.GebSpec
import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import memsource.demo.mock.MemsourceMockServer
import memsource.demo.pages.ProjectsPage
import memsource.demo.pages.SetupPage
import spock.lang.Shared
import static memsource.demo.mock.MemsourceMockServer.*

@Integration
@Rollback
class NavigationFunctionalSpec extends GebSpec {

    void "test navigation to projects page"() {

        setup:"Start at setup page"
            to SetupPage

        when:"Click on 'Projects' in navigation"
            navigation.projectsLink.click()

        then:"Navigated to project page"
            at ProjectsPage

    }

    void "test navigation to setup page"() {

        setup:"Start at projects page"
            to ProjectsPage

        when:"Click on 'Projects' in navigation"
            navigation.setupLink.click()

        then:"Navigated to setup page"
            at SetupPage

    }


}
