package memsource.demo

import grails.testing.mixin.integration.Integration
import grails.transaction.*

import geb.spock.*
import memsource.demo.pages.SetupPage
import static memsource.demo.mock.MemsourceMockServer.*

@Integration
@Rollback
class SetupFunctionalSpec extends GebSpec {

    void "save configuration successfully"() {

        setup:
            to SetupPage

        when:"Fill and submit correct credentials"
            storeConfig(USERNAME, PASSWORD)

        then:"Success message is displayed"
            alerts.messages.size() == 1
            alerts.messages.firstElement().text == 'Configuration stored successfully'

        and:"Username input is filled"
            usernameInput.value() == USERNAME

        and:"Password input is empty"
            passwordInput.value() == ''

    }

    void "save empty configuration"() {

        setup:
            to SetupPage

        when:"Submit empty credentials"
            storeConfig('', '')

        then:"Nothing happens since browser denies submission of empty required input fields"
            alerts.all.empty

    }

}
