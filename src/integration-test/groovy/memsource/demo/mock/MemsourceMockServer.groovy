package memsource.demo.mock

import org.mockserver.integration.ClientAndServer
import org.mockserver.model.JsonBody
import static org.mockserver.model.HttpRequest.request
import static org.mockserver.model.HttpResponse.response

@Singleton(lazy = true)
class MemsourceMockServer {

    @Delegate
    ClientAndServer mockServer = createMockServer()

    static final Integer PORT = 58888
    static final String USERNAME = 'correctUsername'
    static final String PASSWORD = 'correctPassword'
    static final String TOKEN = 'correctToken'
    static final List<String> LANGS = ['en', 'cs', 'sk', 'fr', 'de', 'it', 'es', 'cn', 'jp', 'ru']
    static final List<String> STATUSES = ['NEW', 'ASSIGNED', 'COMPLETED', 'ACCEPTED_BY_VENDOR', 'DECLINED_BY_VENDOR', 'COMPLETED_BY_VENDOR', 'CANCELLED']
    static final FIRST_PROJECT = [
            name: "The First Project <script>alert('Hello')</script>",
            status: 'NEW',
            sourceLang: 'en',
            targetLangs: ['cs','sk','ru']
    ]
    static final Integer PROJECT_COUNT = 20

    static generateProjects(Integer number = 1) {
        (1..number).collect {num ->
            [
                    name: "Project $num" as String,
                    status: STATUSES[num % STATUSES.size()],
                    sourceLang: LANGS[num % LANGS.size()],
                    targetLangs: (1..(num % 4) + 1).collect { LANGS[(num * it) % LANGS.size()] }
            ]
        }
    }

    static ClientAndServer createMockServer() {

        ClientAndServer mockServer = ClientAndServer.startClientAndServer(PORT)

//        mockServer.upsert(OpenAPIExpectation.openAPIExpectation('memsource-api.yaml'))

        mockServer.when(
                request()
                        .withMethod("POST")
                        .withPath("/web/api2/v1/auth/login")
                        .withBody(JsonBody.json([
                                userName: USERNAME,
                                password: PASSWORD,
                        ]))
        ).respond(
                response()
                        .withStatusCode(200)
                        .withBody(JsonBody.json([
                                token: TOKEN,
                        ]))
        )

        mockServer.when(
                request()
                        .withMethod("POST")
                        .withPath("/web/api2/v1/auth/login")
        ).respond(
                response()
                        .withStatusCode(401)
                        .withBody(JsonBody.json([
                                errorCode: "AuthInvalidCredentials",
                                errorDescription: "Unauthorized",
                        ]))
        )

        mockServer.when(
                request()
                        .withMethod("GET")
                        .withPath("/web/api2/v1/projects")
                        .withQueryStringParameter('token', TOKEN)
        ).respond(
                response()
                        .withStatusCode(200)
                        .withBody(JsonBody.json([
                                content: [FIRST_PROJECT] + generateProjects(PROJECT_COUNT - 1),
                                totalElements: PROJECT_COUNT,
                        ]))
        )

        mockServer.when(
                request()
                        .withMethod("GET")
                        .withPath("/web/api2/v1/projects")
        ).respond(
                response()
                        .withStatusCode(401)
                        .withBody(JsonBody.json([
                                errorCode: "AuthInvalidCredentials",
                                errorDescription: "Unauthorized",
                        ]))
        )

        return mockServer

    }

}
