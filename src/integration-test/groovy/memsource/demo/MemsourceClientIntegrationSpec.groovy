package memsource.demo

import groovyx.net.http.HttpResponseException
import grails.gorm.transactions.Rollback
import grails.testing.mixin.integration.Integration
import memsource.demo.mock.MemsourceMockServer
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.Shared
import spock.lang.Specification
import static memsource.demo.mock.MemsourceMockServer.*

//TODO: Test timeouts and network errors
@Integration
@Rollback
class MemsourceClientIntegrationSpec extends Specification {

    @Shared
    MemsourceMockServer memsourceMockServer = MemsourceMockServer.instance

    @Autowired
    MemsourceClient memsourceClient

    void "login with correct credentials"() {

        when:"Call login on Memsource API"
            def response = memsourceClient.login(USERNAME, PASSWORD)

        then:"Response containing token is returned"
            response.token == TOKEN

    }

    void "login with wrong credentials"() {

        when:"Call login on Memsource API"
            memsourceClient.login('wrongUsername', 'wrongPassword')

        then:"HTTP exception with status 401 is thrown"
            HttpResponseException e = thrown()
            e.statusCode == 401

    }

    void "list projects with correct token"() {

        when:"Call list projects on Memsource API"
            def response = memsourceClient.listProjects(TOKEN)

        then:"Response containing token is returned"
            response.content.size() == PROJECT_COUNT
            response.content[0] == FIRST_PROJECT

    }

    void "list projects with wrong token"() {

        when:"Call list projects on Memsource API"
            memsourceClient.listProjects('wrongToken')

        then:"HTTP exception with status 401 is thrown"
            HttpResponseException e = thrown()
            e.statusCode == 401

    }

}
