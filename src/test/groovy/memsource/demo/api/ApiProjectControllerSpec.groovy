package memsource.demo.api

import grails.testing.web.controllers.ControllerUnitTest
import groovy.json.JsonSlurper
import memsource.demo.MemsourceService
import memsource.demo.Project
import spock.lang.Specification

class ApiProjectControllerSpec extends Specification implements ControllerUnitTest<ApiProjectController> {

    private static final List<Project> PROJECTS = (1..3).collect{
        new Project(
                name: "Project $it" as String,
                status: 'NEW',
                sourceLang: 'en',
                targetLangs: ['cs','sk','ru']
        )
    }

    def setup() {
        controller.memsourceService = Mock(MemsourceService)
    }

    void "list action (success)"() {

        setup:"Setup MemsourcService to succeed"
            controller.memsourceService.projects >> PROJECTS

        when:"Call list action"
            controller.list()

        then:"Responds with HTTP 200"
            response.status == 200

        and:"Response body have correct content"
            response.header('Content-Type').startsWith 'application/json'
            def jsonResponse = new JsonSlurper().parseText(response.text)
            jsonResponse.size == PROJECTS.size()
            jsonResponse.items.size() == PROJECTS.size()
            jsonResponse.items == PROJECTS.collect {
                it.properties.subMap(['name', 'status', 'sourceLang', 'targetLangs'])
            }

    }

    void "list action (error)"() {

        setup:"Setup MemsourcService to fail"
            controller.memsourceService.projects >> { throw new Exception('Exception message') }

        when:"Call list action"
            controller.list()

        then:"Responds with HTTP 500"
            response.status == 500

        and:"Response contains error message"
            response.header('Content-Type').startsWith 'application/json'
            response.text == '{"message":"controller.api.project.list.error"}'

    }

}
