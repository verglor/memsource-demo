package memsource.demo

import grails.testing.gorm.DomainUnitTest
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification
import static memsource.demo.ConfigService.Key

class ConfigServiceSpec extends Specification implements ServiceUnitTest<ConfigService>, DomainUnitTest<Config> {

    private static final String KEY = 'testKey'
    private static final String VALUE = 'testValue'
    private static final String USERNAME = 'testUsername'
    private static final String PASSWORD = 'testPassword'


    void "getValue()"() {

        setup:
        new Config(configKey: KEY, configValue: VALUE).save()

        expect:
        service.getValue(KEY) == VALUE

    }

    void "setValue()"() {

        when:
            service.setValue(KEY, VALUE)

        then:
            Config.get(KEY).configValue == VALUE

    }

    void "getUsername()"() {

        setup:
        new Config(configKey: Key.USERNAME, configValue: USERNAME).save()

        expect:
        service.getUsername() == USERNAME

    }

    void "setUsername()"() {

        when:
            service.setUsername(USERNAME)

        then:
            Config.get(Key.USERNAME).configValue == USERNAME

    }

    void "getPassword()"() {

        setup:
            new Config(configKey: Key.PASSWORD, configValue: PASSWORD).save()

        expect:
            service.getPassword() == PASSWORD

    }

    void "setPassword()"() {

        when:
            service.setPassword(PASSWORD)

        then:
            Config.get(Key.PASSWORD).configValue == PASSWORD

    }

    void "setUsernameAndPassword()"() {

        when:
            service.setUsernameAndPassword(USERNAME, PASSWORD)

        then:
            Config.get(Key.USERNAME).configValue == USERNAME
            Config.get(Key.PASSWORD).configValue == PASSWORD

    }

}
