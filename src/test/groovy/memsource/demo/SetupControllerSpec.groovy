package memsource.demo

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class SetupControllerSpec extends Specification implements ControllerUnitTest<SetupController> {

    private static final String USERNAME = 'testUsername'
    private static final String PASSWORD = 'testPassword'

    def setup() {
        controller.configService = Mock(ConfigService)
        controller.memsourceService = Mock(MemsourceService)
    }

    void "index action"() {

        setup:
            controller.configService.username >> USERNAME

        when:"Call index action"
            controller.index()

        then:"Model is filled with correct values"
            model.username == USERNAME

    }

    void "save action (success)"() {

        when:"Call save action"
            controller.save(USERNAME, PASSWORD)

        then:"Store provided username and password"
            1 * controller.configService.setUsernameAndPassword(USERNAME, PASSWORD)

        and:"Evict token"
            1 * controller.memsourceService.evictToken()

        and:"Save success message to flash scope"
            flash.message == 'controller.setup.save.success'

        and:"Redirect to root"
            response.redirectedUrl == '/'

    }

    void "save action (error)"() {

        when:"Call save action"
            controller.save(USERNAME, PASSWORD)

        then:
            1 * controller.configService.setUsernameAndPassword(USERNAME, PASSWORD) >> { throw new Exception() }

        and:"Save error message to flash scope"
            flash.errors == ['controller.setup.save.error']

        and:"Redirect to root"
            response.redirectedUrl == '/'

    }

}
