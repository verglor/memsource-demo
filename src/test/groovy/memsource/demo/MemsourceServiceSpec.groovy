package memsource.demo

import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

class MemsourceServiceSpec extends Specification implements ServiceUnitTest<MemsourceService>{

    private static final String USERNAME = 'testUsername'
    private static final String PASSWORD = 'testPassword'
    private static final String TOKEN = 'testToken'
    private static final List<Map> PROJECT_MAP_LIST = (1..3).collect{
        [
            name: "Project $it" as String,
            status: 'NEW',
            sourceLang: 'en',
            targetLangs: ['cs','sk','ru']
        ]
    }

    def setup() {
        service.memsourceClient = Mock(MemsourceClient)
        service.memsourceClient.login(USERNAME, PASSWORD) >> [token: TOKEN]
        service.configService = Mock(ConfigService)
        service.configService.getUsername() >> USERNAME
        service.configService.getPassword() >> PASSWORD
    }

    void "getToken()"() {

        when:"Request token"
            def token = service.getToken()

        then:"Memsource API is called"
            1 * service.memsourceClient.login(USERNAME, PASSWORD) >> [token: TOKEN]
            token == TOKEN

    }

    void "getProjects()"() {

        when:"Request projects"
            def projects = service.getProjects()

        then:"Memsource API is called"
            1 * service.memsourceClient.listProjects(TOKEN) >> [content: PROJECT_MAP_LIST]

        and:"Returned projects correspondent to result from client"
            projects.size() == PROJECT_MAP_LIST.size()
            projects == PROJECT_MAP_LIST.collect{ new Project(it) }

    }

}
