# Memsource Demo

## How to run this application ?

#### Run in development mode
- `./grailsw run-app`

#### Run in production mode
- `./grailsw prod run-app`

#### Build and run executable WAR
- `./grailsw war`
- `java -jar build/libs/memsource-demo-0.1.war`

## How to use this application ?

- After running the application navigate browser to http://localhost:8080.
- Enter your [Memsource Cloud](https://cloud.memsource.com) credentials on "[Setup](http://localhost:8080/)" page.
- Navigate to "[Projects](http://localhost:8080/projects)" page and enjoy the list of your projects.

## How to test this application ?

- `./gradlew -Dgeb.env=chromeHeadless -Dcodenarc.reportFormat=html check`
- check test reports in `./build/reports/tests/index.html`
- check codenarc report in `./build/reports/codenarc/main.html`

## Where is my configuration stored ?

- In development mode configuration is stored in memory therefore it is erased after application is shut down.

- In production mode mode it is stored in H2 database in file `prodDb.mv.db` in working directory.
